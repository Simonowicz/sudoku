import model.sudoku.Difficulty;
import model.sudoku.SudokuBoard;
import org.apache.log4j.Logger;
import org.jconfig.Configuration;
import org.jconfig.ConfigurationManager;
import solver.SudokuSolver;


/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 31.10.13
 * Time: 02:59
 */
public class Main {
    private static final Configuration configuration = ConfigurationManager.getConfiguration();

    public static void main(String [] args) {
        Logger logger = Logger.getLogger("SudokuLogger");

        SudokuBoard sudokuBoard = new SudokuBoard();
        SudokuSolver solver = new SudokuSolver();
        if(args.length < 1) {
            logger.error("Not enough parameters, solving for empty board, example call: --generate EASY");
        } else {
            String type = args[0];
            if (type.contains("--generate")) {
                String difficulty = args[1];
                try {
                    sudokuBoard.generateRandomBoard(Difficulty.valueOf(difficulty));
                } catch (Exception e) {
                    logger.error("Couldn't generate board with specified difficulty: " + difficulty);
                }
            }
            else if (type.contains("--file")) {
                String fileName = args[1];
                try {
                    sudokuBoard.readFromFile(fileName);
                } catch (Exception e) {
                    logger.error("Couldn't read the board from specified file: " + fileName);
                }
            }
            else if (type.contains("--automatic")) {
                for(Difficulty difficulty : Difficulty.values()) {
                    int fastest = 0;
                    logger.info("Starting tests for " + difficulty.toString() + " difficulty.");
                    for (int i = 0; i < 10000; i++) {
                        sudokuBoard.generateRandomBoard(difficulty);
                        solver.setSudokuBoard(sudokuBoard);
                        long currentTime = System.currentTimeMillis();
                        String boardString = sudokuBoard.toString();
                        solver.solveSudoku();
                        currentTime = System.currentTimeMillis() - currentTime;
                        if (solver.getIterations() ==
                                81 - Integer.parseInt(configuration.getProperty(difficulty.toString().toLowerCase(), null, "difficulties"))) {
                            fastest++;
                        }
                        if (currentTime > 100) {
                            logger.warn("Unusually long processing time " + currentTime + " for the following board: " + boardString);
                        } else {
                            logger.info(currentTime);
                        }
                    }
                    logger.info("Finished tests. Fastest = " + fastest + ", not fastest = " + (10000 - fastest));
                }
                return;
            }
        }

        solver.setSudokuBoard(sudokuBoard);
        logger.info("The board:\n" + sudokuBoard.toString());
        solver.solveSudoku();
        logger.info("The board after solving:\n" + sudokuBoard.toString());
        logger.info("Iterations needed: " + solver.getIterations());
    }
}
