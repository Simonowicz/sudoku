package solver;

import model.utils.Index;

/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 07.11.13
 * Time: 01:17
 */
public class Solution implements Comparable {
    private int cellValue;
    private Index index;
    private int variants;

    public int getCellValue() {
        return cellValue;
    }

    public void setCellValue(int cellValue) {
        this.cellValue = cellValue;
    }

    public Index getIndex() {
        return index;
    }

    public void setIndex(Index index) {
        this.index = index;
    }

    public int getVariants() {
        return variants;
    }

    public void setVariants(int variants) {
        this.variants = variants;
    }

    @Override
    public int compareTo(Object o) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if (!(o instanceof Solution))
            throw new ClassCastException();

        Solution other = (Solution) o;

        if(this.variants > other.getVariants()) {
            return AFTER;
        }
        if(this.variants < other.getVariants()) {
            return BEFORE;
        }

        return EQUAL;
    }
}
