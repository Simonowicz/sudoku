package solver;

import model.sudoku.SudokuBoard;
import model.utils.Index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 31.10.13
 * Time: 11:12
 */
public class SudokuSolver {
    private SudokuBoard sudokuBoard;
    private int iterations = 0;

    public SudokuSolver() {}

    public void setSudokuBoard(SudokuBoard sudokuBoard) {
        this.sudokuBoard = sudokuBoard;
        iterations = 0;
    }

    public int getIterations() {
        return iterations;
    }

    public void solveSudoku() {
        if(sudokuBoard.isFilled()) {
            return;
        }
        try {
            iterations++;
            List<Solution> solutions = generateSolutions();
            for(Solution solution : solutions) {
                sudokuBoard.setCellValue(solution.getCellValue(), solution.getIndex());
                solveSudoku();
                if(sudokuBoard.isFilled()) {
                    return;
                } else {
                    sudokuBoard.resetCellValue(solution.getIndex());
                    if (solution.getVariants() == 1) {
                        return;
                    }
                }
            }
        } catch (SudokuConflictException ignored) {}
    }

    private List<Solution> generateSolutions() throws SudokuConflictException {
        findPossibleNumbers();
        if (!isValidSudoku()) {
            throw new SudokuConflictException();
        }
        findUniqueNumbers();
        return generateOrderedSolutionList();
    }

    private boolean isValidSudoku() {
        for (int i = 1; i < sudokuBoard.variantsPerCell(); i++) {
            if (!isValidColumn(i)) {
                return false;
            }
            if (!isValidRow(i)) {
                return false;
            }
            if (!isValidBlock(i)) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidColumn(final int column) {
        int emptyCells = 0;
        List<Integer> allPossibleNumbers = new ArrayList<>();
        for (int i = 1; i < sudokuBoard.variantsPerCell(); i++) {
            if (!sudokuBoard.getCell(i, column).isFilled()) {
                emptyCells++;
                List<Integer> possibleFromCell = new ArrayList<>(sudokuBoard.getCell(i, column).getPossibleNumbers());
                possibleFromCell.removeAll(allPossibleNumbers);
                allPossibleNumbers.addAll(possibleFromCell);
            }
        }

        return allPossibleNumbers.size() >= emptyCells;
    }

    private boolean isValidRow(final int row) {
        int emptyCells = 0;
        List<Integer> allPossibleNumbers = new ArrayList<>();
        for (int i = 1; i < sudokuBoard.variantsPerCell(); i++) {
            if (!sudokuBoard.getCell(row, i).isFilled()) {
                emptyCells++;
                List<Integer> possibleFromCell = new ArrayList<>(sudokuBoard.getCell(row, i).getPossibleNumbers());
                possibleFromCell.removeAll(allPossibleNumbers);
                allPossibleNumbers.addAll(possibleFromCell);
            }
        }

        return allPossibleNumbers.size() >= emptyCells;
    }

    private boolean isValidBlock(final int blockNumber) {
        Index startIndex = getStartIndex(blockNumber);
        int emptyCells = 0;
        List<Integer> allPossibleNumbers = new ArrayList<>();
        for (int i = startIndex.getRow(); i < startIndex.getRow() + sudokuBoard.getBlockSize(); i++) {
            for (int j = startIndex.getColumn(); j < startIndex.getColumn() + sudokuBoard.getBlockSize(); j++) {
                if (!sudokuBoard.getCell(i, j).isFilled()) {
                    emptyCells++;
                    List<Integer> possibleFromCell = new ArrayList<>(sudokuBoard.getCell(i, j).getPossibleNumbers());
                    possibleFromCell.removeAll(allPossibleNumbers);
                    allPossibleNumbers.addAll(possibleFromCell);
                }
            }
        }

        return allPossibleNumbers.size() >= emptyCells;
    }

    private Index getStartIndex(final int row) {
        switch (row) {
            case 1: return new Index(1, 1);
            case 2: return new Index(1, 4);
            case 3: return new Index(1, 7);
            case 4: return new Index(4, 1);
            case 5: return new Index(4, 4);
            case 6: return new Index(4, 7);
            case 7: return new Index(7, 1);
            case 8: return new Index(7, 4);
            case 9: return new Index(7, 7);
        }
        return null;
    }

    private List<Solution> generateOrderedSolutionList() {
        List<Solution> orderedSolutions = new ArrayList<>();

        Index index = new Index(1, 0);
        for(int i = 0; i < sudokuBoard.numberOfCells(); i++) {
            index = sudokuBoard.nextCell(index.getRow(), index.getColumn());
            if (!sudokuBoard.getCell(index).isFilled())    {
                orderedSolutions.addAll(generateSolutionsForCell(index));
            }
        }

        Collections.sort(orderedSolutions);

        int solutionsToTake = orderedSolutions.get(0).getVariants();
        List<Solution> solutions = new ArrayList<>();
        for(int i = 0; i < solutionsToTake; i++) {
            solutions.add(orderedSolutions.get(i));
        }

        return solutions;
    }

    private List<Solution> generateSolutionsForCell(Index index) {
        List<Solution> solutions = new ArrayList<>();

        for(Integer i : sudokuBoard.getCell(index).getUniqueNumbers()) {
            Solution solution = new Solution();
            solution.setIndex(index);
            solution.setCellValue(i);
            solution.setVariants(sudokuBoard.getCell(index).getUniqueNumbers().size());

            solutions.add(solution);
        }

        List<Integer> possibleNumbersExclusive = new ArrayList<>(sudokuBoard.getCell(index).getPossibleNumbers());
        possibleNumbersExclusive.removeAll(sudokuBoard.getCell(index).getUniqueNumbers());
        for(Integer i : possibleNumbersExclusive) {
            Solution solution = new Solution();
            solution.setIndex(index);
            solution.setCellValue(i);
            solution.setVariants(sudokuBoard.getCell(index).getPossibleNumbers().size());

            solutions.add(solution);
        }

        return solutions;
    }

    private void findUniqueNumbers() {
        Index index = new Index(1, 0);
        for(int i = 0; i < sudokuBoard.numberOfCells(); i++) {
            index = sudokuBoard.nextCell(index.getRow(), index.getColumn());
            if (!sudokuBoard.getCell(index).isFilled()) {
                setUniqueNumbers(index);
            }
        }
    }

    private void setUniqueNumbers(Index index) {
        List<Integer> uniqueNumbers = new ArrayList<>(sudokuBoard.getCell(index).getPossibleNumbers());
        if (uniqueNumbers.size() == 1) {
            sudokuBoard.getCell(index).setUniqueNumbers(uniqueNumbers);
            return;
        }
        List<Integer> uniqueNumbersHelper = getUniqueNumbersFromBox(index, sudokuBoard.getCell(index).getPossibleNumbers());
        if (uniqueNumbersHelper.size() != 0) { // this comparision is needed so that uniqueNumbers are not empty
            uniqueNumbers = new ArrayList<>(uniqueNumbersHelper);
        }

        uniqueNumbersHelper = getUniqueNumbersFromColumn(index, sudokuBoard.getCell(index).getPossibleNumbers());
        if (uniqueNumbersHelper.size() != 0 && uniqueNumbersHelper.size() < uniqueNumbers.size()) {
            uniqueNumbers = new ArrayList<>(uniqueNumbersHelper);
        }

        uniqueNumbersHelper = getUniqueNumbersFromRow(index, sudokuBoard.getCell(index).getPossibleNumbers());
        if (uniqueNumbersHelper.size() != 0 && uniqueNumbersHelper.size() < uniqueNumbers.size()) {
            uniqueNumbers = new ArrayList<>(uniqueNumbersHelper);
        }

        sudokuBoard.getCell(index).setUniqueNumbers(uniqueNumbers);
    }

    public List<Integer> getUniqueNumbersFromBox(Index index, final List<Integer> uniqueNumbers) {
        Index indexHelper = getBoxStartIndex(index);
        List<Integer> uniqueNumbersCopy = new ArrayList<>(uniqueNumbers);

        for (int i = indexHelper.getRow(); i < indexHelper.getRow() + sudokuBoard.getBlockSize(); ++i) {
            for (int j = indexHelper.getColumn(); j < indexHelper.getColumn() + sudokuBoard.getBlockSize(); ++j) {
                if (!sudokuBoard.getCell(i, j).isFilled() && ( i != index.getRow() || j != index.getColumn() )) {
                    uniqueNumbersCopy.removeAll(sudokuBoard.getCell(i, j).getPossibleNumbers());
                    if (uniqueNumbersCopy.size() == 0) {
                        return uniqueNumbersCopy;
                    }
                }
            }
        }

        return uniqueNumbersCopy;
    }

    private List<Integer> getUniqueNumbersFromColumn(Index index, final List<Integer> uniqueNumbers) {
        List<Integer> uniqueNumbersCopy = new ArrayList<>(uniqueNumbers);
        for(int i = 1; i <= sudokuBoard.variantsPerCell(); i++) {
            if (!sudokuBoard.getCell(i, index.getColumn()).isFilled() && i != index.getRow()) {
                uniqueNumbersCopy.removeAll(sudokuBoard.getCell(i, index.getColumn()).getPossibleNumbers());
                if (uniqueNumbersCopy.size() == 0) {
                    return uniqueNumbersCopy;
                }
            }
        }
        return uniqueNumbersCopy;
    }

    private List<Integer> getUniqueNumbersFromRow(Index index, final List<Integer> uniqueNumbers) {
        List<Integer> uniqueNumbersCopy = new ArrayList<>(uniqueNumbers);
        for(int i = 1; i <= sudokuBoard.variantsPerCell(); i++) {
            if (!sudokuBoard.getCell(index.getRow(), i).isFilled() && i != index.getColumn()) {
                uniqueNumbersCopy.removeAll(sudokuBoard.getCell(index.getRow(), i).getPossibleNumbers());
                if (uniqueNumbersCopy.size() == 0) {
                    return uniqueNumbersCopy;
                }
            }
        }
        return uniqueNumbersCopy;
    }

    private void findPossibleNumbers() throws SudokuConflictException {
        Index index = new Index(1, 0);
        for(int i = 0; i < sudokuBoard.numberOfCells(); i++) {
            index = sudokuBoard.nextCell(index.getRow(), index.getColumn());
            if (!sudokuBoard.getCell(index).isFilled()) {
                setPossibleNumbers(index);
            }
        }
    }

    private void setPossibleNumbers(Index index) throws SudokuConflictException {
        List<Integer> possibleNumbers = initPossibleNumbers();
        excludeNumbersFromBox(index, possibleNumbers);
        excludeNumbersFromColumn(index, possibleNumbers);
        excludeNumbersFromRow(index, possibleNumbers);
        if (possibleNumbers.size() == 0) {
            throw new SudokuConflictException();
        }
        sudokuBoard.getCell(index).setPossibleNumbers(possibleNumbers);
    }

    private List<Integer> initPossibleNumbers() {
        List<Integer> possibleNumbers = new ArrayList<>();
        for (int i = 1; i <= sudokuBoard.variantsPerCell(); i++) {
            possibleNumbers.add(i);
        }
        return possibleNumbers;
    }

    private void excludeNumbersFromBox(Index index, List<Integer> possibleNumbers) {
        Index indexHelper = getBoxStartIndex(index);

        for (int i = indexHelper.getRow(); i < indexHelper.getRow() + sudokuBoard.getBlockSize(); ++i) {
            for (int j = indexHelper.getColumn(); j < indexHelper.getColumn() + sudokuBoard.getBlockSize(); ++j) {
                if (sudokuBoard.getCell(i, j).isFilled()) {
                    possibleNumbers.remove(new Integer(sudokuBoard.getCell(i, j).get()));
                }
            }
        }
    }

    private Index getBoxStartIndex(Index index) {
        Index indexHelper = new Index();
        if (index.getRow() % sudokuBoard.getBlockSize() == 0) {
            indexHelper.setRow(index.getRow() - sudokuBoard.getBlockSize() + 1);
        } else {
            indexHelper.setRow((index.getRow() / sudokuBoard.getBlockSize()) * sudokuBoard.getBlockSize() + 1);
        }
        if (index.getColumn() % sudokuBoard.getBlockSize() == 0) {
            indexHelper.setColumn(index.getColumn() - sudokuBoard.getBlockSize() + 1);
        } else {
            indexHelper.setColumn((index.getColumn() / sudokuBoard.getBlockSize()) * sudokuBoard.getBlockSize() + 1);
        }
        return indexHelper;
    }

    private void excludeNumbersFromColumn(Index index, List<Integer> possibleNumbers) {
        for(int i = 1; i <= sudokuBoard.variantsPerCell(); i++) {
            if (sudokuBoard.getCell(i, index.getColumn()).isFilled()) {
                possibleNumbers.remove(new Integer(sudokuBoard.getCell(i, index.getColumn()).get()));
            }
        }
    }

    private void excludeNumbersFromRow(Index index, List<Integer> possibleNumbers) {
        for(int i = 1; i <= sudokuBoard.variantsPerCell(); i++) {
            if (sudokuBoard.getCell(index.getRow(), i).isFilled()) {
                possibleNumbers.remove(new Integer(sudokuBoard.getCell(index.getRow(), i).get()));
            }
        }
    }
}
