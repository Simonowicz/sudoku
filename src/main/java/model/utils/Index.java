package model.utils;

/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 31.10.13
 * Time: 01:40
 */
public class Index {
    private int row;
    private int column;

    public Index() {}

    public Index(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
