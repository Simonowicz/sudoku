package model.sudoku;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 31.10.13
 * Time: 00:45
 */
public class SudokuCell {

    private int value;
    private boolean filled;
    private List<Integer> tried;
    private List<Integer> possibleNumbers;
    private List<Integer> uniqueNumbers;

    public SudokuCell() {

        filled = false;
        tried = new ArrayList<>();
        possibleNumbers = new ArrayList<>();
        uniqueNumbers = new ArrayList<>();
    }

    public boolean isFilled() {
        return filled;
    }

    public int get() {
        return value;
    }

    public void set(final int number) {
        filled = true;
        value = number;
        tried.add(number);
    }

    public void clear() {
        value = 0;
        filled = false;
    }

    public void reset() {
        clear();
        tried.clear();
    }

    public boolean isTried(final int number) {
        return tried.contains(number);
    }

    public void tryNumber(final int number) {
        tried.add(number);
    }

    public int numberOfTried() {
        return tried.size();
    }

    public List<Integer> getPossibleNumbers() {
        return possibleNumbers;
    }

    public void setPossibleNumbers(List<Integer> possibleNumbers) {
        this.possibleNumbers = possibleNumbers;
    }

    public List<Integer> getUniqueNumbers() {
        return uniqueNumbers;
    }

    public void setUniqueNumbers(List<Integer> uniqueNumbers) {
        this.uniqueNumbers = uniqueNumbers;
    }
}