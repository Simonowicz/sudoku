package model.sudoku;

/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 31.10.13
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */
public enum Difficulty {
    VERY_EASY,
    EASY,
    MEDIUM,
    HARD,
    EVIL
}
