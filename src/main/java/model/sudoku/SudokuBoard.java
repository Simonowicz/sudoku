package model.sudoku;

import model.utils.Index;
import org.apache.log4j.Logger;
import org.jconfig.Configuration;
import org.jconfig.ConfigurationManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Szymciu
 * Date: 31.10.13
 * Time: 00:50
 */
public class SudokuBoard {
    Logger logger = Logger.getLogger("SudokuLogger");

    private static final Configuration configuration = ConfigurationManager.getConfiguration();
    private static final int STANDARD_SIZE = 3;
    private final int blockSize;
    private final int boardSize;
    private SudokuCell[][] cells;
    private Random rng = new Random();
    private int filledCells;

    public SudokuBoard() {
        this(STANDARD_SIZE);
    }

    public SudokuBoard(final int blockSize) {
        this.blockSize = blockSize;
        boardSize = blockSize * blockSize;
        initializeBoard();
    }

    private void initializeBoard() {
        cells = new SudokuCell[boardSize][boardSize];
        for (int i = 0; i < boardSize; ++i) {
            for (int j = 0; j < boardSize; ++j) {
                cells[i][j] = new SudokuCell();
            }
        }
    }

    public int variantsPerCell() {
        return boardSize;
    }

    public int getBlockSize() {
        return blockSize;
    }

    public int numberOfCells() {
        return boardSize * boardSize;
    }

    public SudokuCell getCell(final int row, final int column) {
        return cells[row - 1][column - 1];
    }

    public SudokuCell getCell(Index index) {
        return cells[index.getRow() - 1][index.getColumn() - 1];
    }

    public void setCellValue(final int number, final Index index) {
        setCellValue(number, index.getRow(), index.getColumn());
    }

    public void setCellValue(final int number, final int row, final int column) {
        filledCells++;
        cells[row - 1][column - 1].set(number);
    }

    public void resetCellValue(final Index index) {
        resetCellValue(index.getRow(), index.getColumn());
    }

    public void resetCellValue(final int row, final int column) {
        filledCells--;
        cells[row - 1][column - 1].reset();
    }

    public boolean isFilled() {
        return filledCells == boardSize * boardSize;
    }

    public boolean checkNumberBox(final int number, int row, int column) {
        if (row % blockSize == 0) {
            row -= blockSize - 1;
        } else {
            row = (row / blockSize) * blockSize + 1;
        }
        if (column % blockSize == 0) {
            column -= blockSize - 1;
        } else {
            column = (column / blockSize) * blockSize + 1;
        }
        for (int i = row; i < row + blockSize; ++i) {
            for (int j = column; j < column + blockSize; ++j) {
                if (cells[i - 1][j - 1].isFilled() && (cells[i - 1][j - 1].get() == number)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkNumberRow(final int number, final int row) {
        for (int i = 0; i < boardSize; ++i) {
            if (cells[row - 1][i].isFilled() && cells[row - 1][i].get() == number) {
                return false;
            }
        }
        return true;
    }

    public boolean checkNumberColumn(final int number, final int column) {
        for (int i = 0; i < boardSize; ++i) {
            if (cells[i][column - 1].isFilled() && cells[i][column - 1].get() == number) {
                return false;
            }
        }
        return true;
    }

    public boolean checkNumberField(final int number, final int row, final int column) {
        return (checkNumberBox(number, row, column)
                && checkNumberRow(number, row)
                && checkNumberColumn(number, column));
    }

    public Index nextCell(final int row, final int column) {
        int r = row, c = column;
        if (c < boardSize) {
            ++c;
        } else {
            c = 1;
            ++r;
        }
        return new Index(r, c);
    }

    public int getRandomNumber() {
        return rng.nextInt(boardSize) + 1;
    }

    public Index getRandomIndex() {
        int column, row;
        column = rng.nextInt(boardSize);
        row = rng.nextInt(boardSize);

        return new Index(row, column);
    }

    public void generateRandomBoard(Difficulty difficulty) {
        generateRandomBoard();
        String filledFieldsAmountString = configuration.getProperty(difficulty.toString().toLowerCase(), null, "difficulties");
        Integer filledFieldsAmount = Integer.parseInt(filledFieldsAmountString);
        filledCells = filledFieldsAmount;
        removeFields(filledFieldsAmount);
    }

    private void removeFields(Integer destinatedAmount) {
        int amountToBeDeleted = boardSize * boardSize - destinatedAmount;
        for (int i = 0; i < amountToBeDeleted; i++) {
            Index index;
            do {
               index = getRandomIndex();
            } while (!cells[index.getRow()][index.getColumn()].isFilled());
            cells[index.getRow()][index.getColumn()].reset();
        }
    }

    public void generateRandomBoard() {
        generateRandomBoard(1, 1);
    }

    private void generateRandomBoard(final int row, final int column) {
        if (!cells[boardSize-1][boardSize-1].isFilled()) {
            while (cells[row - 1][column - 1].numberOfTried() < this.variantsPerCell()) {
                int candidate;
                do {
                    candidate = getRandomNumber();
                } while (cells[row - 1][column - 1].isTried(candidate));
                if (this.checkNumberField(candidate, row, column)) {
                    cells[row-1][column-1].set(candidate);
                    Index nextCell = this.nextCell(row, column);
                    generateRandomBoard(nextCell.getRow(), nextCell.getColumn());
                } else {
                    cells[row - 1][column - 1].tryNumber(candidate);
                }
            }
            if (!cells[boardSize-1][boardSize-1].isFilled()) {
                cells[row - 1][column - 1].reset();
            }
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for(int i = 0; i < boardSize; i++) {
            for(int j = 0; j < boardSize; j++)
                stringBuilder.append(cells[i][j].get()).append(" ");
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    public void readFromFile(String fileName) {
        BufferedReader br = null;
        try {
            int lineNumber = 0;
            String sCurrentLine;
            br = new BufferedReader(new FileReader(fileName));
            while ((sCurrentLine = br.readLine()) != null) {
                sCurrentLine = sCurrentLine.replaceAll(" ", "");
                for(int i = 0; i < boardSize; i++) {
                    if (Character.getNumericValue(sCurrentLine.charAt(i)) != 0) {
                        cells[lineNumber][i].set(Character.getNumericValue(sCurrentLine.charAt(i)));
                        filledCells++;
                    }
                }
                lineNumber++;
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
        }
    }
}